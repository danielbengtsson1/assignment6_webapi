﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Asignment6_WebApi.Migrations
{
    public partial class initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(130)", maxLength: 130, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(130)", maxLength: 130, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(130)", maxLength: 130, nullable: true),
                    PictureUrl = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.CharacterId);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(350)", maxLength: 350, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.FranchiseId);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(130)", maxLength: 130, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(130)", maxLength: 130, nullable: true),
                    ReleaseYear = table.Column<string>(type: "nvarchar(110)", maxLength: 110, nullable: true),
                    Director = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    PictureUrl = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: true),
                    TrailerUrl = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "FranchiseId");
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "Alias", "FullName", "Gender", "PictureUrl" },
                values: new object[,]
                {
                    { 1, "Greenleaf", "Legolas", "Elf", "Not set" },
                    { 2, "Saruman the White", "Saruman", "Maiar", "Not set" },
                    { 3, "Disturber of the Peace", "Bilbo Baggins", "Male", "Not set" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The story about all the events that occur in J.R.R Tolkiens Middle Earth.", "The Lord of the Rings" },
                    { 2, "The wacky adventures of our favourite group of superheroes in the marvel universe.", "Marvel Cinematic Universe" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "MovieTitle", "PictureUrl", "ReleaseYear", "TrailerUrl" },
                values: new object[,]
                {
                    { 1, "Peter Jackson", 1, "Fantasy", "Lord of the Rings: The Fellowship of the Ring", "Not set", "2001", "Not set" },
                    { 2, "Peter Jackson", 1, "Fantasy", "Lord of the Rings: The Two Towers", "Not set", "2002", "Not set" },
                    { 3, "Peter Jackson", 1, "Fantasy", "Lord of the Rings: The Return of the King", "Not set", "2003", "Not set" },
                    { 4, "Peter Jackson", 1, "Fantasy", "The Hobbit: An Unexpected Journey", "Not set", "2012", "Not set" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 3 },
                    { 2, 4 },
                    { 3, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
