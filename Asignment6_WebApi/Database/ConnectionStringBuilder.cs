﻿using Microsoft.Data.SqlClient;

namespace Asignment6_WebApi.Database
{
    public class ConnectionStringBuilder
    {
        public static string GetConnectionString(string serverName)
        {
            SqlConnectionStringBuilder builder = new();

            builder.DataSource = serverName;
            builder.InitialCatalog = "FranchiseDb";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;

            return builder.ConnectionString;
        }

        public ConnectionStringBuilder()
        {

        }

    }
}
