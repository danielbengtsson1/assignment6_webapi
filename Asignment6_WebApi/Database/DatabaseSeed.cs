﻿using Asignment6_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Asignment6_WebApi.Database
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            //Movies
            Movie fellowship = new()
            {
                MovieId = 1,
                MovieTitle = "Lord of the Rings: The Fellowship of the Ring",
                Genre = "Fantasy",
                ReleaseYear = "2001",
                Director = "Peter Jackson",
                PictureUrl = "Not set",
                TrailerUrl = "Not set",
                FranchiseId = 1

            };
            Movie towers = new()
            {
                MovieId = 2,
                MovieTitle = "Lord of the Rings: The Two Towers",
                Genre = "Fantasy",
                ReleaseYear = "2002",
                Director = "Peter Jackson",
                PictureUrl = "Not set",
                TrailerUrl = "Not set",
                FranchiseId = 1,

            };
            Movie king = new()
            {
                MovieId = 3,
                MovieTitle = "Lord of the Rings: The Return of the King",
                Genre = "Fantasy",
                ReleaseYear = "2003",
                Director = "Peter Jackson",
                PictureUrl = "Not set",
                TrailerUrl = "Not set",
                FranchiseId = 1

            };
            Movie hobbit = new()
            {
                MovieId = 4,
                MovieTitle = "The Hobbit: An Unexpected Journey",
                Genre = "Fantasy",
                ReleaseYear = "2012",
                Director = "Peter Jackson",
                PictureUrl = "Not set",
                TrailerUrl = "Not set",
                FranchiseId = 1,
            };

            //Franchises
            Franchise lotr = new()
            {
                FranchiseId = 1,
                Name = "The Lord of the Rings",
                Description = "The story about all the events that occur in J.R.R Tolkiens Middle Earth.",
            };
            Franchise mcu = new()
            {
                FranchiseId = 2,
                Name = "Marvel Cinematic Universe",
                Description = "The wacky adventures of our favourite group of superheroes in the marvel universe."
            };

            //Characters
            Character legolas = new()
            {
                CharacterId = 1,
                FullName = "Legolas",
                Alias = "Greenleaf",
                Gender = "Elf",
                PictureUrl = "Not set",


            };
            Character saruman = new()
            {
                CharacterId = 2,
                FullName = "Saruman",
                Alias = "Saruman the White",
                Gender = "Maiar",
                PictureUrl = "Not set",

            };
            Character bilbo = new()
            {
                CharacterId = 3,
                FullName = "Bilbo Baggins",
                Alias = "Disturber of the Peace",
                Gender = "Male",
                PictureUrl = "Not set",

            };


            modelBuilder.Entity<Franchise>().HasData(lotr, mcu);

            modelBuilder.Entity<Movie>().HasData(fellowship, towers, king, hobbit);

            modelBuilder.Entity<Character>().HasData(legolas, saruman, bilbo);


            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 1, CharacterId = 3 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 2 },
                            new { MovieId = 4, CharacterId = 3 }
                        );
                    });

        }

    }
}
