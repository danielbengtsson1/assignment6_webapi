﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Asignment6_WebApi.Database.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly FranchiseDbContext _context;

        public MovieRepository(FranchiseDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> Create(Movie movie)
        {
            if (movie != null) await _context.Movies.AddAsync(movie);
            
            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return movie ?? throw new NullReferenceException();
        }

        public async Task<bool> Delete(int id)
        {
            Movie movie = await Read(id);

            if (movie == null) return false;

            _context.Movies.Remove(movie);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return true;
        }

        public async Task<List<Movie>> Read()
        {
            return await _context.Movies.Include(m => m.Franchise).Include(m => m.Characters).ToListAsync();
        }

        public async Task<Movie> Read(int id)
        {
            Movie? movie = await _context.Movies
                .Include(m => m.Franchise)
                .Include(m => m.Characters!)
                .ThenInclude(c => c.Movies)
                .SingleOrDefaultAsync(m => m.MovieId == id);

            return movie ?? throw new NullReferenceException();
        }

        public Movie Update(Movie editedMovie)
        {
            if (editedMovie != null) _context.Movies.Update(editedMovie);

            if (_context.SaveChanges() == 0) throw new Exception("No changes were made in database");

            return editedMovie ?? throw new NullReferenceException();
        }
    }
}
