﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Asignment6_WebApi.Database.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly FranchiseDbContext _context;

        public FranchiseRepository(FranchiseDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> Create(Franchise franchise)
        {
            if (franchise != null) await _context.Franchises.AddAsync(franchise);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return franchise ?? throw new NullReferenceException();
        }

        public async Task<bool> Delete(int id)
        {
            Franchise franchise = await Read(id);

            if (franchise == null) return false;

            _context.Franchises.Remove(franchise);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return true;
        }

        public async Task<List<Franchise>> Read()
        {
            return await _context.Franchises.Include(m => m.Movies).ToListAsync();
        }

        public async Task<Franchise> Read(int id)
        {
            Franchise? franchise = await _context.Franchises
                .Include(f => f.Movies!)
                .ThenInclude(m => m.Characters)
                .SingleOrDefaultAsync(c => c.FranchiseId == id);

            return franchise ?? throw new NullReferenceException();
        }

        public Franchise Update(Franchise editedFranchise)
        {
            if (editedFranchise != null) _context.Franchises.Update(editedFranchise);

            if (_context.SaveChanges() == 0) throw new Exception("No changes were made in database");

            return editedFranchise ?? throw new NullReferenceException();
        }
    }
}
