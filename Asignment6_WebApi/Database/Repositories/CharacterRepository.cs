﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Asignment6_WebApi.Database.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly FranchiseDbContext _context;

        public CharacterRepository(FranchiseDbContext context)
        {
            _context = context;
        }

        public async Task<Character> Create(Character character)
        {
            if (character != null) await _context.Characters.AddAsync(character);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return character is not null ? character : throw new NullReferenceException();
        }

        public async Task<bool> Delete(int id)
        {
            Character character = await Read(id);

            if (character == null) return false;

            _context.Characters.Remove(character);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No changes were made in database");

            return true;
        }

        public async Task<List<Character>> Read()
        {
           return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }

        public async Task<Character> Read(int id)
        {
            Character? character = await _context.Characters.Include(c => c.Movies).SingleOrDefaultAsync(c => c.CharacterId == id) ;

            return character is not null ? character : throw new NullReferenceException();
        }

        public Character Update(Character editedCharacter)
        {
            if (editedCharacter != null) _context.Characters.Update(editedCharacter);

            if (_context.SaveChanges() == 0) throw new Exception("No changes were made in database");

            return editedCharacter is not null ?  editedCharacter : throw new NullReferenceException();
        }
    }
}
