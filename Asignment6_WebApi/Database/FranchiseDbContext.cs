﻿using Asignment6_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Asignment6_WebApi.Database
{
    public class FranchiseDbContext : DbContext
    {

        public DbSet<Character> Characters { get; set; } = null!;
        public DbSet<Movie> Movies { get; set; } = null!;
        public DbSet<Franchise> Franchises { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Insert the name of your SSMS server as a string here ─────────────────────────────────-┐
            //                                                                                        |
            //                                                                                        V
            optionsBuilder.UseSqlServer(ConnectionStringBuilder.GetConnectionString("----< YOUR SQL SERVER NAME >----"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            ModelBuilderExtensions.Seed(modelBuilder);
        }
    }
}
