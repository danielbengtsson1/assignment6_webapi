﻿using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;

namespace Asignment6_WebApi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, ReadCharacterDTO>()
                .ForMember(
                    c => c.Movies,
                    opt => opt.MapFrom(src => src.Movies!.Select(m => m.MovieId).ToArray())
                    ).ReverseMap();
            CreateMap<CreateCharacterDTO, Character>().ReverseMap();
            CreateMap<UpdateCharacterDTO, Character>().ReverseMap();
        }
    }
}
