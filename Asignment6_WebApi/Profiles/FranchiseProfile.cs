﻿using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;

namespace Asignment6_WebApi.Profiles
{
    public class FranchiseProfile: Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, ReadFranchiseDTO>()
               .ForMember(
                   c => c.Movies,
                   opt => opt.MapFrom(src => src.Movies!.Select(m => m.MovieId).ToArray())
                   ).ReverseMap();
            CreateMap<CreateFranchiseDTO, Franchise>().ReverseMap();
            CreateMap<UpdateFranchiseDTO, Franchise>().ReverseMap();
        }
    }
}
