﻿using Asignment6_WebApi.Models.DTOs;
using Asignment6_WebApi.Models;
using AutoMapper;

namespace Asignment6_WebApi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, ReadMovieDTO>()
                .ForMember(
                    m => m.FranchiseId,
                    opt => opt.MapFrom(src => src.Franchise!.FranchiseId)
                    )
                .ForMember(
                    m => m.Characters,
                    opt => opt.MapFrom(src => src.Characters!.Select(c => c.CharacterId).ToArray())
                    ).ReverseMap();
            CreateMap<CreateMovieDTO, Movie>().ReverseMap();
            CreateMap<UpdateMovieDTO, Movie>().ReverseMap();
        }
    }
}
