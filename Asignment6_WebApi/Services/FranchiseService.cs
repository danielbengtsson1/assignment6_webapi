﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;

namespace Asignment6_WebApi.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly IFranchiseRepository _franchiseRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;

        public FranchiseService(IFranchiseRepository franchiseRepository, IMovieRepository movieRepository, IMapper mapper)
        {
            _franchiseRepository = franchiseRepository;
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        public async Task<Franchise> Add(CreateFranchiseDTO domainFranchise)
        {
            var franchise = _mapper.Map<Franchise>(domainFranchise);

            return await _franchiseRepository.Create(franchise);
        }

        public async Task<ReadFranchiseDTO> Edit(int id, UpdateFranchiseDTO editedFranchise)
        {
            var oldFranchise = await _franchiseRepository.Read(id);

            if (oldFranchise == null) throw new Exception("Could not find entity in database.");

            _mapper.Map(editedFranchise, oldFranchise);

            return _mapper.Map<ReadFranchiseDTO>(_franchiseRepository.Update(oldFranchise));
        }

        public async Task<ReadFranchiseDTO> FindById(int id)
        {
            var franchise = await _franchiseRepository.Read(id);

            if (franchise == null) throw new Exception("Franchise not found in Database.");
            if (franchise.Movies != null)
            {
                foreach (var movie in franchise.Movies)
                {
                    movie.Characters = null;
                }
            }

            return _mapper.Map<ReadFranchiseDTO>(franchise);
        }

        public async Task<ICollection<ReadFranchiseDTO>> GetAll()
        {
            return _mapper.Map<List<ReadFranchiseDTO>>(await _franchiseRepository.Read());
        }

        public async Task<List<ReadCharacterDTO>> GetAllCharactersFromFranchise(int franchiseId)
        {
            var franchise = await _franchiseRepository.Read(franchiseId);
            List<ReadCharacterDTO> characterList = new();

            if (franchise == null) throw new Exception("Franchise not found in Database.");
            if (franchise.Movies != null)
            {
                foreach (var movie in franchise.Movies)
                {
                    if (movie.Characters != null)
                    {
                        foreach (var character in movie.Characters)
                        {
                            characterList.Add(_mapper.Map<ReadCharacterDTO>(character));
                        }
                    }
                }
            }

            characterList = characterList.DistinctBy(x => x.CharacterId).ToList();

            return characterList;
        }

        public async Task<bool> Remove(int id)
        {
            return await _franchiseRepository.Delete(id);
        }

        public async Task<ReadFranchiseDTO> UpdateMovies(int franchiseId, int[] movieIds)
        {
            var franchise = await _franchiseRepository.Read(franchiseId);

            if (franchise == null) throw new Exception("Franchise not found in Database.");
            if (franchise.Movies != null)
            {
                franchise.Movies.Clear();

                foreach (var id in movieIds)
                {
                    franchise.Movies.Add(await _movieRepository.Read(id));
                }
            }
            _franchiseRepository.Update(franchise);

            return _mapper.Map<ReadFranchiseDTO>(franchise);
        }
    }
}
