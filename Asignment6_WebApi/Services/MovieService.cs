﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;

namespace Asignment6_WebApi.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;


        public MovieService(IMovieRepository movieRepository, IMapper mapper, ICharacterRepository characterRepository)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
            _characterRepository = characterRepository;
        }

        public async Task<Movie> Add(CreateMovieDTO domainMovie)
        {
            var movie = _mapper.Map<Movie>(domainMovie);

            return await _movieRepository.Create(movie);
        }

        public async Task<ReadMovieDTO> Edit(int id, UpdateMovieDTO editedMovie)
        {
            var oldMovie = await _movieRepository.Read(id);

            if (oldMovie == null) throw new Exception("Could not find entity in database.");

            _mapper.Map(editedMovie, oldMovie);

            return _mapper.Map<ReadMovieDTO>(_movieRepository.Update(oldMovie));
        }

        public async Task<ReadMovieDTO> EditCharactersInMovie(int[] characterIds, int movieId)
        {
            var movie = await _movieRepository.Read(movieId);

            if (movie == null) throw new Exception("Movie not found in database.");

            if (movie.Characters != null)
            {
                movie.Characters.Clear();

                foreach (var id in characterIds)
                {
                    movie.Characters.Add(await _characterRepository.Read(id));
                }
            }

            _movieRepository.Update(movie);

            return _mapper.Map<ReadMovieDTO>(movie);
        }

        public async Task<ReadMovieDTO> FindById(int id)
        {
            var movie = await _movieRepository.Read(id);

            if (movie == null) throw new Exception("Movie not found in database.");
            if (movie.Franchise != null) movie.Franchise.Movies = null;

            return _mapper.Map<ReadMovieDTO>(movie);
        }

        public async Task<ICollection<ReadMovieDTO>> GetAll()
        {
            var movieList = await _movieRepository.Read();

            foreach (Movie movie in movieList)
            {
                if (movie.Franchise != null) movie.Franchise.Movies = null;
            }

            return _mapper.Map<List<ReadMovieDTO>>(movieList);
        }

        public async Task<List<ReadCharacterDTO>> GetAllCharactersFromMovie(int movieId)
        {
            Movie movie = await _movieRepository.Read(movieId);
            List<ReadCharacterDTO> characters = new();

            if (movie == null) throw new Exception("Movie not found in database.");
            if (movie.Characters != null)
            {
                foreach (var character in movie.Characters)
                {
                    characters.Add(_mapper.Map<ReadCharacterDTO>(character));
                }
            }

            return characters;
        }

        public async Task<bool> Remove(int id)
        {
            return await _movieRepository.Delete(id);
        }
    }
}
