﻿using Asignment6_WebApi.Database.Repositories;
using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;

namespace Asignment6_WebApi.Database.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;
        public CharacterService(ICharacterRepository characterRepository, IMapper mapper)
        {
            _characterRepository = characterRepository;
            _mapper = mapper;
        }

        public async Task<Character> Add(CreateCharacterDTO character)
        {
            return await _characterRepository.Create(_mapper.Map<Character>(character));
        }

        public async Task<ReadCharacterDTO> Edit(int id, UpdateCharacterDTO editedCharacter)
        {
            var oldCharacter = await _characterRepository.Read(id);

            if (oldCharacter == null) throw new Exception("Character not found in database.");

            _mapper.Map(editedCharacter, oldCharacter);

            return _mapper.Map<ReadCharacterDTO>(_characterRepository.Update(oldCharacter));
        }

        public async Task<ReadCharacterDTO> FindById(int id)
        {
            var character = await _characterRepository.Read(id);

            if (character == null) throw new Exception("Character not found in database.");

            return _mapper.Map<ReadCharacterDTO>(character);
        }

        public async Task<ICollection<ReadCharacterDTO>> GetAll()
        {
            return _mapper.Map<List<ReadCharacterDTO>>(await _characterRepository.Read());
        }

        public async Task<bool> Remove(int id)
        {
            return await _characterRepository.Delete(id);
        }
    }
}
