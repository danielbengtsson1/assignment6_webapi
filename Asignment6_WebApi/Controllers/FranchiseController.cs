﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Asignment6_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private IFranchiseService _franchiseService;

        public FranchiseController(IFranchiseService franchiseService)
        {
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Get all franchises from the database.
        /// </summary>
        // GET: api/<FranchiseController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetAll()
        {
            return Ok(await _franchiseService.GetAll());
        }


        /// <summary>
        /// Get a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        // GET api/<FranchiseController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDTO>> GetFranchise(int id)
        {
            return Ok(await _franchiseService.FindById(id));
        }


        /// <summary>
        /// Create and add a new franchise to the database.
        /// </summary>
        /// <param name="franchiseDTO"></param>
        // POST api/<FranchiseController>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(CreateFranchiseDTO franchiseDTO)
        {
            var franchise = await _franchiseService.Add(franchiseDTO);

            return CreatedAtAction(
                "GetFranchise",
                new { id = franchise.FranchiseId },
                franchiseDTO
                );
        }


        /// <summary>
        /// Edit the properties of a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateFranchise"></param>
        // PUT api/<FranchiseController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, UpdateFranchiseDTO updateFranchise)
        {
            await _franchiseService.Edit(id, updateFranchise);

            return NoContent();
        }


        /// <summary>
        /// Delete a franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/<FranchiseController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var didDelete = await _franchiseService.Remove(id);

            if (!didDelete) return NotFound();

            return Ok();
        }


        /// <summary>
        /// Get all the characters from a specific franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        //Get All Characters in a Franchise
        [HttpGet("charactersFromfranchise/{franchiseId}")]
        public async Task<ActionResult<List<ReadCharacterDTO>>> GetAllCharactersFromFranchise(int franchiseId)
        {
            return await _franchiseService.GetAllCharactersFromFranchise(franchiseId);
        }


        /// <summary>
        /// Edit which movies are a part of a specified franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieIds"></param>
        //Update movies in a franchise
        [HttpPut("UpdateMovies/{franchiseId}")]
        public async Task<ReadFranchiseDTO> UpdateMovies(int franchiseId, int[] movieIds)
        {
            return await _franchiseService.UpdateMovies(franchiseId, movieIds);
        }
    }
}
