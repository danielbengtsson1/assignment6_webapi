﻿using Asignment6_WebApi.Database.Services;
using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Asignment6_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Get all movies from the database.
        /// </summary>
        // GET: api/<MovieController>
        [HttpGet]
        public async Task<ActionResult<ICollection<ReadMovieDTO>>> GetAll()
        {
            return Ok(await _movieService.GetAll());
        }

        /// <summary>
        /// Get a specific movie from the database.
        /// </summary>
        /// <param name="id"></param>
        // GET api/<MovieController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadMovieDTO>> GetMovie(int id)
        {
            return Ok(await _movieService.FindById(id));
        }

        /// <summary>
        /// Create and add a new movie to the database. /*/ SET [franchiseId] TO NULL IF YOU DONT WANT A FRANCHISE TIED TO THE MOVIE \*\
        /// </summary>
        /// <param name="movieDTO"></param>
        // POST api/<MovieController>
        [HttpPost]
        public async Task<IActionResult> PostMovie(CreateMovieDTO movieDTO)
        {
            var movie = await _movieService.Add(movieDTO);

            return CreatedAtAction(
                "GetMovie",
                new { id = movie.MovieId },
                movieDTO
                );
        }

        /// <summary>
        /// Edit the properties of a specific movie. /*/ SET [franchiseId] TO NULL IF YOU DONT WANT A FRANCHISE TIED TO THE MOVIE \*\
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateMovie"></param>
        // PUT api/<MovieController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, UpdateMovieDTO updateMovie)
        {
            await _movieService.Edit(id, updateMovie);

            return NoContent();
        }

        /// <summary>
        /// Delete a movie from the database.
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/<MovieController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var didDelete = await _movieService.Remove(id);

            if (!didDelete) return NotFound();

            return Ok();
        }

        /// <summary>
        /// Get all the characters from a specific movie.
        /// </summary>
        /// <param name="movieId"></param>
        //Get All Characters in a Movie
        [HttpGet("CharactersFromMovie/{movieId}")]
        public async Task<ActionResult<List<ReadCharacterDTO>>> GetAllCharactersFromMovie(int movieId)
        {
            return await _movieService.GetAllCharactersFromMovie(movieId);
        }

        /// <summary>
        /// Edit which characters takes part in a specified movie.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="movieId"></param>
        //Update Characters in movie
        [HttpPut("updateCharactersmovie/{movieId}")]
        public async Task<ReadMovieDTO> UpdateCharacters(int[] characterIds, int movieId)
        {
            return await _movieService.EditCharactersInMovie(characterIds, movieId);
        }

    }
}
