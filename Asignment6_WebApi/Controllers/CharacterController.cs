﻿using Asignment6_WebApi.Interfaces;
using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;


namespace Asignment6_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Get all characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<ReadCharacterDTO>>> GetAll()
        {
            return Ok(await _characterService.GetAll());
        }

        /// <summary>
        ///     Get specific character with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadCharacterDTO>> GetCharacter(int id)
        {
            return Ok(await _characterService.FindById(id));
        }

        /// <summary>
        ///     Post a character to Database
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CreateCharacterDTO characterDTO)
        {
            var domainCharacter = await _characterService.Add(characterDTO);

            return CreatedAtAction(
                "GetCharacter",
                new { id = domainCharacter.CharacterId },
                characterDTO
                );
        }

        /// <summary>
        ///     Update a character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, UpdateCharacterDTO updateCharacter)
        {
            await _characterService.Edit(id, updateCharacter);

            return NoContent();
        }
        /// <summary>
        ///     Remove character from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<CharacterController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            bool didDelete = await _characterService.Remove(id);

            if (!didDelete) return NotFound();

            return Ok();
        }
    }
}
