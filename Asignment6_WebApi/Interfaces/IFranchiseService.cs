﻿using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;

namespace Asignment6_WebApi.Interfaces
{
    public interface IFranchiseService
    {
        /// <summary>
        ///     Get all franchises in database.
        /// </summary>
        /// <returns>A list of all the franchises.</returns>
        public Task<ICollection<ReadFranchiseDTO>> GetAll();
        /// <summary>
        ///     Get a specific franchise by id.
        /// </summary>
        /// <param name="id">ID of wanted franchise.</param>
        /// <returns>A object with the franchise with given ID.</returns>
        public Task<ReadFranchiseDTO> FindById(int id);
        /// <summary>
        ///     Create a franchise.
        /// </summary>
        /// <param name="franchise">The franchise data to add.</param>
        /// <returns>A franchise object.</returns>
        public Task<Franchise> Add(CreateFranchiseDTO franchise);
        /// <summary>
        ///     Edit an existing franchise.
        /// </summary>
        /// <param name="id">ID of the franchise to edit</param>
        /// <param name="franchise">The data to update.</param>
        /// <returns>A object of the edited franchise.</returns>
        public Task<ReadFranchiseDTO> Edit(int id, UpdateFranchiseDTO franchise);
        /// <summary>
        ///     Remove a franchise with specific id.
        /// </summary>
        /// <param name="id">ID of the franchise that will be removed.</param>
        /// <returns>A boolean value.</returns>
        public Task<bool> Remove(int id);
        /// <summary>
        ///     Get all the characters from a specific franchise.
        /// </summary>
        /// <param name="franchiseId">ID of the franchise to get the characters from.</param>
        /// <returns>A list of characters in a specific franchise.</returns>
        public Task<List<ReadCharacterDTO>> GetAllCharactersFromFranchise(int franchiseId);
        /// <summary>
        ///     Update movies in franchise.
        /// </summary>
        /// <param name="franchiseId">ID of the franchise.</param>
        /// <param name="movieIds">IDs of the movies.</param>
        /// <returns>A object with the updated movies.</returns>
        public Task<ReadFranchiseDTO> UpdateMovies(int franchiseId, int[] movieIds);
    }
}
