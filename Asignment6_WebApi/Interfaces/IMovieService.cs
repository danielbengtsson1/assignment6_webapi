﻿using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;

namespace Asignment6_WebApi.Interfaces
{
    public interface IMovieService
    {
        /// <summary>
        ///     Add a movie to database.
        /// </summary>
        /// <param name="movie">The movie data to add.</param>
        /// <returns>A movie object</returns>
        public Task<Movie> Add(CreateMovieDTO movie);
        /// <summary>
        ///     Edit a existing movie.
        /// </summary>
        /// <param name="id">ID of the movie to edit.</param>
        /// <param name="movie">The updated movie.</param>
        /// <returns>The updated movie object.</returns>
        public Task<ReadMovieDTO> Edit(int id, UpdateMovieDTO movie);
        /// <summary>
        ///     Get all movies.
        /// </summary>
        /// <returns>A lists of all movies.</returns>
        public Task<ICollection<ReadMovieDTO>> GetAll();
        /// <summary>
        ///     Find a specific movie with id.
        /// </summary>
        /// <param name="id">ID of the movie to find.</param>
        /// <returns>The specific Movie.</returns>
        public Task<ReadMovieDTO> FindById(int id);
        /// <summary>
        ///     Remove a specific movie with ID
        /// </summary>
        /// <param name="id">ID of the movie to remove.</param>
        /// <returns>A boolean value.</returns>
        public Task<bool> Remove(int id);
        /// <summary>
        ///     Get the characters from a specific movie.
        /// </summary>
        /// <param name="movieId">ID of the movie with the characters.</param>
        /// <returns>A list of all the characters in a specific movie.</returns>
        public Task<List<ReadCharacterDTO>> GetAllCharactersFromMovie(int movieId);
        /// <summary>
        ///     Edit the characters in a specific movie.
        /// </summary>
        /// <param name="characterIds">IDs of characters.</param>
        /// <param name="movieId">ID of the movie to edit characters.</param>
        /// <returns>An object with the updated characters in a movie.</returns>
        public Task<ReadMovieDTO> EditCharactersInMovie(int[] characterIds, int movieId);


    }
}
