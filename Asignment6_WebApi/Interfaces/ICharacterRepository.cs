﻿using Asignment6_WebApi.Models;

namespace Asignment6_WebApi.Interfaces
{
    public interface ICharacterRepository
    {
        /// <summary>
        /// Takes in a Character object and saves it to the DbContext.
        /// </summary>
        /// <param name="character">The Character object</param>
        /// <returns>The Character object that was saved</returns>
        public Task<Character> Create(Character character);
        /// <summary>
        /// Gets all the Character objects from the database.
        /// </summary>
        /// <returns>A List of Character objects</returns>
        public Task<List<Character>> Read();
        /// <summary>
        /// Takes in a Character Id int and gets the correlating Character 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Character object</param>
        /// <returns>The specified Character object</returns>
        public Task<Character> Read(int id);
        /// <summary>
        /// Takes in the original Character object from the database with updated
        /// properties and updates that entity to the database.
        /// </summary>
        /// <param name="editedCharacter">The old Character object with updated properties</param>
        /// <returns>The updated Character object</returns>
        public Character Update(Character editedCharacter);
        /// <summary>
        /// Takes in a Character Id int and removes the correlating Character 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Character object</param>
        /// <returns>A bool that indicates if the operation was successful</returns>
        public Task<bool> Delete(int id);
    }
}
