﻿using Asignment6_WebApi.Models;
using Asignment6_WebApi.Models.DTOs;

namespace Asignment6_WebApi.Interfaces
{
    public interface ICharacterService
    {
        /// <summary>
        ///     Get all the Characters.
        /// </summary>
        /// <returns>A list of all the characters.</returns>
        public Task<ICollection<ReadCharacterDTO>> GetAll();
        /// <summary>
        ///     Get a specific character.
        /// </summary>
        /// <param name="id">ID of the wanted character.</param>
        /// <returns>A object with character.</returns>
        public Task<ReadCharacterDTO> FindById(int id);
        /// <summary>
        ///     Add a new character to the database.
        /// </summary>
        /// <param name="character">The character data to be added.</param>
        /// <returns>A object with the added character.</returns>
        public Task<Character> Add(CreateCharacterDTO character);
        /// <summary>
        ///     Edit an existing character.
        /// </summary>
        /// <param name="id">ID of the character to be edited.</param>
        /// <param name="character">the data to edit.</param>
        /// <returns>A object with the updated character.</returns>
        public Task<ReadCharacterDTO> Edit(int id, UpdateCharacterDTO character);
        /// <summary>
        ///     Remove a character.
        /// </summary>
        /// <param name="id">ID of the character to be removed.</param>
        /// <returns>A boolean value.</returns>
        public Task<bool> Remove(int id);

    }
}
