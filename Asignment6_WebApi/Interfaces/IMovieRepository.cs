﻿using Asignment6_WebApi.Models;

namespace Asignment6_WebApi.Interfaces
{
    public interface IMovieRepository
    {
        /// <summary>
        /// Takes in a Movie object and saves it to the DbContext.
        /// </summary>
        /// <param name="movie">The Movie object</param>
        /// <returns>The Movie object that was saved</returns>
        public Task<Movie> Create(Movie movie);
        public Task<List<Movie>> Read();
        /// <summary>
        /// Gets all the Movie objects from the database.
        /// </summary>
        /// <returns>A List of Movie objects</returns>
        /// <summary>
        /// Takes in a Movie Id int and gets the correlating Movie 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Movie object</param>
        /// <returns>The specified Movie object</returns>
        public Task<Movie> Read(int id);
        /// <summary>
        /// Takes in the original Movie object from the database with updated
        /// properties and updates that entity to the database.
        /// </summary>
        /// <param name="editedMovie">The old Movie object with updated properties</param>
        /// <returns>The updated Movie object</returns>
        public Movie Update(Movie editedMovie);
        /// <summary>
        /// Takes in a Movie Id int and removes the correlating Movie 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Movie object</param>
        /// <returns>A bool that indicates if the operation was successful</returns>
        public Task<bool> Delete(int id);
    }
}
