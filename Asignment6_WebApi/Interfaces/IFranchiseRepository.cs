﻿using Asignment6_WebApi.Models;

namespace Asignment6_WebApi.Interfaces
{
    public interface IFranchiseRepository
    {
        /// <summary>
        /// Takes in a Franchise object and saves it to the DbContext.
        /// </summary>
        /// <param name="franchise">The Franchise object</param>
        /// <returns>The Franchise object that was saved</returns>
        public Task<Franchise> Create(Franchise franchise);
        /// <summary>
        /// Gets all the Franchise objects from the database.
        /// </summary>
        /// <returns>A List of Franchise objects</returns>
        public Task<List<Franchise>> Read();
        /// <summary>
        /// Takes in a Franchise Id int and gets the correlating Character 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Franchise object</param>
        /// <returns>The specified Franchise object</returns>
        public Task<Franchise> Read(int id);
        /// <summary>
        /// Takes in the original Franchise object from the database with updated
        /// properties and updates that entity to the database.
        /// </summary>
        /// <param name="editedFranchise">The old Franchise object with updated properties</param>
        /// <returns>The updated Franchise object</returns>
        public Franchise Update(Franchise editedFranchise);
        /// <summary>
        /// Takes in a Franchise Id int and removes the correlating Franchise 
        /// object from the database.
        /// </summary>
        /// <param name="id">The int identifier for the Franchise object</param>
        /// <returns>A bool that indicates if the operation was successful</returns>
        public Task<bool> Delete(int id);
    }
}
