﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models
{
    public class Character
    {
        [Required]
        public int CharacterId { get; set; }

        [Required]
        [MaxLength(130)]
        public string FullName { get; set; }

        [MaxLength(130)]
        public string? Alias { get; set; }

        [MaxLength(130)]
        public string? Gender { get; set; }

        [MaxLength(800)]
        public string? PictureUrl { get; set; }

        public ICollection<Movie>? Movies { get; set; }

        public Character()
        {
            FullName = "I was not set at intitialization";
        }
    }
}
