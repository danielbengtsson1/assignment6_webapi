﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models
{
    public class Movie
    {
        [Required]
        public int MovieId { get; set; }

        [Required]
        [MaxLength(130)]
        public string MovieTitle { get; set; } 

        [MaxLength(130)]
        public string? Genre { get; set; }

        [MaxLength(110)]
        public string? ReleaseYear { get; set; }

        [MaxLength(120)]
        public string? Director { get; set; }

        [MaxLength(800)]
        public string? PictureUrl { get; set; }

        [MaxLength(800)]
        public string? TrailerUrl { get; set; }

        public int? FranchiseId { get; set; }

        public Franchise? Franchise { get; set; }

        public ICollection<Character>? Characters { get; set; }

        public Movie()
        {
            MovieTitle = "I was not set at intitialization";
        }
    }
}
