﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models.DTOs
{
    public class ReadMovieDTO
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }

        public string? Genre { get; set; }

        public string? ReleaseYear { get; set; }

        public string? Director { get; set; }

        public string? PictureUrl { get; set; }

        public string? TrailerUrl { get; set; }

        public int? FranchiseId { get; set; }

        public Franchise? Franchise { get; set; }

        public ICollection<int>? Characters { get; set; }
        public ReadMovieDTO()
        {
            MovieTitle = "I was not set at intitialization";
        }
    }
}
