﻿namespace Asignment6_WebApi.Models.DTOs
{
    public class ReadCharacterDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? PictureUrl { get; set; }
        public ICollection<int>? Movies { get; set; }

        public ReadCharacterDTO()
        {
            FullName = "I was not set at intitialization";
        }
    }
}
