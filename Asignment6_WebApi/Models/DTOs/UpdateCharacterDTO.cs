﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models.DTOs
{
    public class UpdateCharacterDTO
    {

        [MaxLength(130)]
        public string FullName { get; set; }

        [MaxLength(130)]
        public string? Alias { get; set; }

        [MaxLength(130)]
        public string? Gender { get; set; }

        [MaxLength(800)]
        public string? PictureUrl { get; set; }

        public UpdateCharacterDTO()
        {
            FullName = "I was not set at intitialization";
        }

    }
}
