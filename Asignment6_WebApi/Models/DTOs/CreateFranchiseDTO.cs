﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models.DTOs
{
    public class CreateFranchiseDTO
    {
        
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(350)]
        public string Description { get; set; }

        public CreateFranchiseDTO()
        {
            Name = "I was not set at intitialization";
            Description = "I was not set at intitialization";
        }
    }
}
