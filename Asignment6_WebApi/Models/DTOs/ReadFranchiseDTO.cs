﻿namespace Asignment6_WebApi.Models.DTOs
{
    public class ReadFranchiseDTO
    {
        public int FranchiseId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public ICollection<int>? Movies { get; set; }
    }
}
