﻿using System.ComponentModel.DataAnnotations;

namespace Asignment6_WebApi.Models
{
    public class Franchise
    {
        [Required]
        public int FranchiseId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(350)]
        public string? Description { get; set; }

        public ICollection<Movie>? Movies { get; set; }

        public Franchise()
        {
            Name = "I was not set at intitialization";
        }
    }
}
