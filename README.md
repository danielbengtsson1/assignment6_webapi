[![forthebadge](https://forthebadge.com/images/badges/made-with-c-sharp.svg)](https://forthebadge.com)
![alt text](/entity-framework.svg)
# Movie characters API 
This API is about movies and characters connected to a SQL server database. 

## Description

This application is an API about movies and the characters in them. API is connected to a SQL server to store the informaton that gets posted through the API. there is also seeded data that gets created at the start of the application.

## Database diagram
![alt text](database-diagram.png)

## Getting Started

### Tools 

* Visual studio
* SQL server management studio
* .NET 6


### Dependencies

* AutoMapper.Extensions.Microsoft
* Microsoft.EntityFrameworkCore
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Tools
* Swashbuckle.AspNetCore
* Newtonsoft.Json


### Executing program

* Run update-database in the PM console to start up the database with the seeded data.
* In the franchiseDBcontext, use your own SQL server name as shown below.
```c#
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
    optionsBuilder.UseSqlServer(ConnectionStringBuilder.GetConnectionString("----YOUR SQL SERVER NAME HERE----"));
}
```


## Authors

[ Marcus Jarlevid ](@Jallemania)
    [LinkedIn](https://www.linkedin.com/in/marcus-jarlevid)

[Daniel Bengtsson ](@DanielBengtsson) 
    [LinkedIn](https://www.linkedin.com/in/daniel-bengtsson-0baa881a3)


### acknowledgement
* This project was developed as an assignment for a course in C# and .NET. Course provided by Noroff Education AS.


